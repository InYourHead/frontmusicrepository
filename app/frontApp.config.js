/**
 * Created by aleksander on 19.02.17.
 */
'use strict';
angular.module('frontApp')
    .config(['$locationProvider', '$routeProvider',
    function ($locationProvider, $routeProvider) {
        //$locationProvider.hashPrefix('');

        $routeProvider
            .when('/login', {
                template: '<login-form></login-form>'
            })
            .when('/search/:id', {
                template: '<result-list></result-list>'
            })
            .when('/search',{
                template: '<result-list></result-list>'
            })
            .when('/artists/:artistName',{
              template: '<artist-view></artist-view>'
            })
          .when('/logout', {
            template: " ",
            controller: 'logoutController'
          })
          .when('/login/:error', {
            template: '<login-form></login-form>'
          })
          .when('/albums/:artistName/:albumTitle', {
            template: '<album-view></album-view>'
          })
          .when('/tracks/:artistName/:albumTitle/:trackTitle', {
            template: '<track-view></track-view>'
          })
          .when('/add', {
            template: '<input-form></input-form>'
          })
          .when('/add/:artistName', {
            template: '<input-form></input-form>'
          })
          .when('/add/:artistName/:albumTitle', {
            template: '<input-form></input-form>'
            })
            .when('/404', {
                template: '<not-found></not-found>'
            })
            .otherwise('/search');

    }]).run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 xediatable theme. Can be also 'bs2', 'default'
}).config(['$httpProvider', function ($httpProvider) {

  $httpProvider.interceptors.push('AuthInterceptor');

}]);
