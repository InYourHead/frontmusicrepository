/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 25.02.17.
 */
describe('trackViewController', function () {

  beforeEach(module('trackView'));

  var trackController, scope, routeParams, $httpBackend, $controller, data, $compile;

  beforeEach(inject(function (_$compile_, _$controller_, _$rootScope_, _$routeParams_, TracksService, _$httpBackend_, $templateCache) {

    data = {trackTitle: 'Curse'};

    $compile = _$compile_;
    $httpBackend = _$httpBackend_;
    $controller = _$controller_;

    scope = _$rootScope_.$new();
    routeParams = {artistName: 'Metallica', albumTitle: 'Metallica', trackTitle: 'Curse'};

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica/albums/Metallica/tracks/Curse')
      .respond(data);


    trackController = $controller('trackController', {
      $scope: scope,
      $routeParams: routeParams,
      TracksService: TracksService
    });

  }));

  it('should check does trackController is defined', function () {

    expect(trackController).toBeTruthy();

  });

  it('should check does artistName is equal to $routeParams.artistName and albumTitle is equal to $routeParams.albumTitle and trackTitle equal to $routeParams.trackTitle', function () {
    jasmine.addCustomEqualityTester(angular.equals);

    $httpBackend.flush();

    expect(scope.artistName).toEqual('Metallica');

    expect(scope.albumTitle).toEqual('Metallica');

    expect(scope.trackTitle).toEqual('Curse');

  });

  it('should check does TracksService is returning track object', function () {

    jasmine.addCustomEqualityTester(angular.equals);

    $httpBackend.flush();

    expect(scope.track).toEqual(data);

  });

  it('should check does delete button works', function () {

    $httpBackend.expectDELETE('http://localhost:8080/artists/Metallica/albums/Metallica/tracks/Curse')
      .respond(200);

    scope.deleteRequest();

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

  it('should check does update button works', function () {

    $httpBackend.expectPUT('http://localhost:8080/artists/Metallica/albums/Metallica/tracks/Curse')
      .respond(200);

    scope.editRequest();

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

});
