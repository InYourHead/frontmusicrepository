/**
 * Created by aleksander on 01.03.17.
 */
'use strict';
angular.module('trackView')
  .component('trackView', {

    templateUrl: 'track-view/track-view.template.html',
    controller: 'trackController'
  });
