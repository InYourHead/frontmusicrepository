/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 24.02.17.
 */
'use strict';
angular.module('trackView')
  .controller('trackController', ['$scope', '$routeParams', 'TracksService', '$location', 'BaseControllerFactory', function ($scope, $routeParams, TracksService, $location, BaseControllerFactory) {

      var baseController= BaseControllerFactory.create($scope);

    var artistName = $routeParams.artistName;

    var albumTitle = $routeParams.albumTitle;

    var trackTitle = $routeParams.trackTitle;

    $scope.artistName = artistName;

    $scope.albumTitle = albumTitle;

    $scope.trackTitle = trackTitle;

    $scope.track = TracksService.get({artistName: artistName, albumTitle: albumTitle, trackTitle: trackTitle});

    $scope.editRequest = function () {

        if($scope.track.trackTitle== ""){
            baseController.notify("Track title can't be empty!", "danger");
        }

        if(!$scope.message)
            TracksService.update({artistName: artistName, albumTitle: albumTitle, trackTitle: trackTitle}, $scope.track).$promise.then(baseController.defaultSaveSuccessHandler, {});
    };

    $scope.deleteRequest = function () {
      var id = {artistName: artistName, albumTitle: albumTitle, trackTitle: trackTitle};
      TracksService.delete(id);
      $location.path('/search');
    }

  }]);
