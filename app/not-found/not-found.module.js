/**
 * Created by aleksander on 12.03.17.
 */
'use strict';
angular.module('notFound', [])
    .component('notFound', {

        templateUrl: 'not-found/not-found.template.html'
    });