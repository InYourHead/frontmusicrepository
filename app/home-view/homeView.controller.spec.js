/**
 * Created by aleksander on 01.03.17.
 */
describe('homeViewController test', function () {

  var mockedAuthManager, mockedCred;

  var $controller, scope, AuthManager, homeController;

  var buttonLoginUrl = "#/login";
  var buttonLogoutUrl = "#/logout";

  var buttonLoginLabel = "Login";
  var buttonLogoutLabel = "Log out";

  /*
   $scope.buttonLabel = buttonLoginLabel;
   $scope.buttonUrl = buttonLoginUrl;


   */

  //mock factory

  mockedAuthManager = {
    getEncodeCredentials: function () {
      return mockedCred;
    },
    setCredentials: function ($l, $p) {
      mockedCred = btoa($l + ':' + $p);
    },
    deleteCredentials: function () {
      mockedCred = undefined;
    }
  };

  beforeEach(function () {

    //potrzebujemy mockować authManager


    module('homeView');

    module('authManager', function ($provide) {

      $provide.factory('AuthManager', function () {
        return mockedAuthManager;
      });
    });

  });


  beforeEach(inject(function (_$controller_, _$rootScope_, _AuthManager_) {

    $controller = _$controller_;
    scope = _$rootScope_.$new();
    AuthManager = _AuthManager_;

    homeController = $controller('homeController', {
      $scope: scope,
      AuthManager: AuthManager
    });

  }));

  it('should check does AuthManager is defined', function () {
    expect(AuthManager).toBeDefined();
  });

  it('should check does homeController is defined', function () {
    expect(homeController).toBeDefined();
  });

  it('should check does default values are set when user is a quest', function () {
    AuthManager.deleteCredentials();

    expect(scope.homeUrl).toEqual('#/search');
    expect(scope.buttonLabel).toEqual(buttonLoginLabel);
    expect(scope.buttonUrl).toEqual(buttonLoginUrl);

  });

  it('should check does values are set, when user is logged in', function () {
    AuthManager.setCredentials('login', 'password');

    //notify $watch
    scope.$digest();

    expect(scope.buttonLabel).toEqual(buttonLogoutLabel);
    expect(scope.buttonUrl).toEqual(buttonLogoutUrl);
  });

  it('should check does values are set to default, when user log out', function () {
    AuthManager.setCredentials('login', 'password');

    //notify $watch
    scope.$digest();

    AuthManager.deleteCredentials();

    //notify $watch
    scope.$digest();

    expect(scope.buttonLabel).toEqual(buttonLoginLabel);
    expect(scope.buttonUrl).toEqual(buttonLoginUrl);

  });

});
