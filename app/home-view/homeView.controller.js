/**
 * Created by aleksander on 20.02.17.
 */
'use strict';
angular.module('homeView')
  .controller('homeController', ['$scope', 'AuthManager', function ($scope, AuthManager) {

    $scope.isLoggedIn = AuthManager.getEncodeCredentials();

    var buttonLoginUrl = "#/login";
    var buttonLogoutUrl = "#/logout";

    var buttonLoginLabel = "Login";
    var buttonLogoutLabel = "Log out";

    //initialize variables

    $scope.homeUrl = '#/search';

    $scope.buttonLabel = buttonLoginLabel;
    $scope.buttonUrl = buttonLoginUrl;

     $scope.$watch (function () {
     return AuthManager.getEncodeCredentials();},
       function (newValue, oldValue) {

         if (newValue) {

           $scope.buttonLabel = buttonLogoutLabel;
           $scope.buttonUrl = buttonLogoutUrl;

         }
         else {

           $scope.buttonLabel = buttonLoginLabel;
           $scope.buttonUrl = buttonLoginUrl;

     }


     });


  }]);
