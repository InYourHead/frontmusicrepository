/**
 * Created by aleksander on 19.02.17.
 */
'use strict';

// Define the `frontApp` module
angular.module('frontApp', [
  'directives',
    'notFound',
  'resultList',
  'services',
  'artistView',
  'albumView',
  'trackView',
  'homeView',
  'loginForm',
  'authInterceptor',
  'logoutController',
  'inputForm',
  'xeditable'
]);
