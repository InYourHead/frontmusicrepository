/**
 * Created by aleksander on 25.02.17.
 */
describe('artistViewController', function () {

  beforeEach(module('artistView'));

  var artistController, scope, routeParams, $httpBackend, $controller, data, $compile;

  beforeEach(inject(function (_$compile_, _$controller_, _$rootScope_, _$routeParams_, ArtistsService, _$httpBackend_, $templateCache) {

    data={artistName:'Metallica', albums:[]};

    $compile = _$compile_;
    $httpBackend=_$httpBackend_;
    $controller=_$controller_;

    scope=_$rootScope_.$new();
    routeParams={artistName: 'Metallica'};

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica')
      .respond(data);


    artistController=$controller('artistController', {
      $scope:scope,
      $routeParams:routeParams,
      ArtistsService:ArtistsService
    });

  }));

  it('should check does artistController is definied', function () {

    expect(artistController).toBeTruthy();

  });

  it('should check does artistName is equal to $routeParams.artistName', function () {
    jasmine.addCustomEqualityTester(angular.equals);

    $httpBackend.flush();

    expect(scope.artist.artistName).toEqual('Metallica');

  });

  it('should check does ArtistsService is returning artist object', function () {

    jasmine.addCustomEqualityTester(angular.equals);

    $httpBackend.flush();

    expect(scope.artist).toEqual(data);

  });

  it('should check does delete button works', function () {

    $httpBackend.expectDELETE('http://localhost:8080/artists/Metallica')
      .respond('');

    scope.deleteRequest();

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

  it('should check does update button works', function () {

    $httpBackend.expectPUT('http://localhost:8080/artists/Metallica')
      .respond('');

    scope.editRequest();

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

});
