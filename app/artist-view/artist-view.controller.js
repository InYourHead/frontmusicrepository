/**
 * Created by aleksander on 24.02.17.
 */
'use strict';
angular.module('artistView')
  .controller('artistController', ['$scope', '$routeParams', 'ArtistsService', '$location', 'BaseControllerFactory', function ($scope, $routeParams, ArtistsService, $location, BaseControllerFactory) {

      var baseController= BaseControllerFactory.create($scope);

    $scope.artistId=$routeParams.artistName;

    $scope.artist=ArtistsService.get({artistName: $scope.artistId});

    $scope.artist.artistName=$routeParams.artistName;

    $scope.editRequest=function () {
      if ($scope.artist.artistName == "") {
          baseController.notify("Artist name can't be empty!", "danger");
      }
      else {
        if ($scope.artist.albums) {
          for (var i = 0; i < $scope.artist.albums.length; i++) {
            if ($scope.artist.albums[i].albumTitle == "") {
                baseController.notify("Album title can't be empty!", "danger");
                 break;
            }
            if ($scope.artist.albums[i].tracks) {
              for (var j = 0; j < $scope.artist.albums[i].tracks.length; j++) {
                if ($scope.artist.albums[i].tracks[j].trackTitle == "") {
                    baseController.notify("Track title can't be empty!", "danger");
                  break;
                }
              }
            }

          }
        }
      }
      if (!$scope.message) {

      ArtistsService.update({artistName: $scope.artistId}, $scope.artist).$promise.then(baseController.defaultSaveSuccessHandler, {});

          $scope.artistId=$scope.artist.artistName;

      }
    };

    $scope.deleteRequest = function () {
      var id = {artistName: $scope.artistId};
      ArtistsService.delete(id);
      $location.path('/search');
    }

  }]);
