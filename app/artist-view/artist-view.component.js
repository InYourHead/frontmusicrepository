/**
 * Created by aleksander on 24.02.17.
 */
'use strict';
angular.module('artistView')
.component('artistView',{

  templateUrl: 'artist-view/artist-view.template.html',
  controller: 'artistController'
});
