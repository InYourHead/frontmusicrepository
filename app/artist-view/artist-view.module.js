/**
 * Created by aleksander on 24.02.17.
 */
'use strict';
angular.module('artistView', [
  'services.artistsService',
    'baseController',
  'ngRoute'
]);
