/**
 * Created by aleksander on 10.03.17.
 */
'use strict';
angular.module('baseController')
  .factory('BaseControllerFactory', [ '$location', '$controller',
    function ( $location, $controller) {

      return {

        create: function ($scope) {

          return $controller('BaseController', {

            $scope: $scope,
            $location: $location
          });

        }

      }
    }]);
