/**
 * Created by aleksander on 10.03.17.
 */
describe('baseController test', function () {

  var ctrl, $scope, $q, $rootScope;

  beforeEach(module('baseController'));

  beforeEach(inject(function ($controller,$location, _$rootScope_, _$q_) {


    $rootScope=_$rootScope_;

    $q=_$q_;

    $scope = _$rootScope_.$new();

    ctrl = $controller('BaseController', {
      $scope: $scope,
      $location: $location,
        $q: _$q_
    });

  }));

  it('fails when BaseController is undefined', function () {

    expect(ctrl).toBeDefined();

  });


  it('fails when artist exists and errorMessage has not been set (POST request)', function () {


    ctrl.saveArtistErrorHandler({status:409});


    expect($scope.message).toEqual('Typed artist already exists');
    expect($scope.messageType).toEqual('Danger!');

  });

  it('fails when album exists and errorMessage has not been set (POST request)', function () {

    ctrl.saveAlbumErrorHandler({status:409});

      expect($scope.message).toEqual('Typed album already exists');
      expect($scope.messageType).toEqual('Danger!');

  });

  it('fails when track exists and and errorMessage has not been set (POST request)', function () {


      ctrl.saveTrackErrorHandler({status: 409});

      expect($scope.message).toEqual('Typed track already exists');
      expect($scope.messageType).toEqual('Danger!');

  });

  it('fails when object was created and successMessage has not been set (POST request)', function () {

    ctrl.defaultSaveSuccessHandler({status: 200});

    expect($scope.message).toEqual("Save completed");
      expect($scope.messageType).toEqual('Success!');

  });

  it('fails when priority == danger and notify function dont work', function () {
      ctrl.notify("message", "danger");

      expect($scope.message).toEqual("message");
      expect($scope.messageType).toEqual("Danger!");
      expect($scope.alertClass).toEqual("alert alert-danger alert-dismissable");
  });

    it('fails when priority == info and notify function dont work', function () {
        ctrl.notify("message", "info");

        expect($scope.message).toEqual("message");
        expect($scope.messageType).toEqual("Info!");
        expect($scope.alertClass).toEqual("alert alert-info alert-dismissable");
    });

    it('fails when priority == warning and notify function dont work', function () {
        ctrl.notify("message", "warning");

        expect($scope.message).toEqual("message");
        expect($scope.messageType).toEqual("Warning!");
        expect($scope.alertClass).toEqual("alert alert-warning alert-dismissable");
    });

    it('fails when priority == success and notify function dont work', function () {
        ctrl.notify("message", "success");

        expect($scope.message).toEqual("message");
        expect($scope.messageType).toEqual("Success!");
        expect($scope.alertClass).toEqual("alert alert-success alert-dismissable");
    });
});

