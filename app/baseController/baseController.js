/**
 * Created by aleksander on 10.03.17.
 */
'use strict';
angular.module('baseController', [])
  .controller('BaseController', ['$scope', '$location', '$q', function ($scope, $location, $q) {

      var self=this;

      this.notify=function (message, priority) {
          var al_class="alert ";
          var messageType="";


          if(priority == "success"){

              al_class= al_class +"alert-success";
              messageType="Success!";
          }
          else if(priority == "info"){

              al_class= al_class + "alert-info";
              messageType="Info!";
          }
          else if(priority == "warning"){
              al_class= al_class + "alert-warning";
              messageType="Warning!";
          }
          else if(priority == "danger"){
              al_class= al_class + "alert-danger";
              messageType="Danger!";
          }

          $scope.message=message;
          $scope.messageType=messageType;
          $scope.alertClass=al_class + " alert-dismissable";

      };

      this.defaultSaveSuccessHandler = function (success) {

         self.notify("Save completed", "success");

      };

    this.saveArtistErrorHandler = function (error) {

        if (error.status == 409) {
            self.notify('Typed artist already exists', "danger");
            return $q.reject({message: 'Typed artist already exists'});
          }
    };

    this.saveAlbumErrorHandler = function (error) {
          if (error.status == 409) {

            self.notify('Typed album already exists',"danger");
            return $q.reject({message: 'Typed album already exists'});
          }
    };

    this.saveTrackErrorHandler = function (error) {

          if (error.status == 409) {
            self.notify('Typed track already exists',"danger");
            return $q.reject({message: 'Typed track already exists'});
          }

    };

  }]);
