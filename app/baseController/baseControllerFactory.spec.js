/**
 * Created by aleksander on 10.03.17.
 */
describe('baseControllerFactory test', function () {


  var factory, $scope;

  beforeEach(module('services', 'baseController'));


  beforeEach(inject(function (BaseControllerFactory, $rootScope) {

    factory = BaseControllerFactory;
    $scope = $rootScope.$new();
  }));

  it('fails when factory is not defined', function () {

    expect(factory).toBeDefined();

  });

  it('fails when create() not creating baseController', function () {

    var ctrl = factory.create($scope);

    expect(ctrl).toBeDefined();

  });


});
