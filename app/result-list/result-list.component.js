/**
 * Created by aleksander on 20.02.17.
 */
'use strict';
angular.module('resultList')
    .component('resultList',{
        templateUrl: 'result-list/result-list.template.html',
        controller: 'resultController'
    });