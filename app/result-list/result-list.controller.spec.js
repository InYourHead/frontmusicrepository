/**
 * Created by aleksander on 21.02.17.
 */
'use strict';
describe('resultList', function () {

    beforeEach(module('resultList'));



    describe('resultController test', function () {

        var ResultController, scope;

        beforeEach(inject(function ($controller, $rootScope) {


            scope=$rootScope.$new();


            ResultController=$controller('resultController', {
                $scope: scope
            });

        }));

        it('controller should create empty array', function () {

            jasmine.addCustomEqualityTester(angular.equals);

            expect(scope.artists).toEqual([]);

        });

      it('should check does Add Artist button is defined correctly', function () {
        expect(scope.addArtistUrl).toEqual('#/add');

      })
    });
});
