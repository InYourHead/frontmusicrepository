/**
 * Created by aleksander on 20.02.17.
 */
'use strict';
angular.module('resultList',[
    'ngRoute',
    'services',
    'ngResource',
    'directives.searchBar'
]);