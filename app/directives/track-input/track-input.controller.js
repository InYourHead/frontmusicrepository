/**
 * Created by aleksander on 04.03.17.
 */
'use strict';
angular.module('directives.trackInput').controller('trackInputController', function ($scope) {

  $scope.newTrack = "";

  $scope.addTrack = function () {

    var newTrackObject = {trackTitle: $scope.newTrack};

    if ($scope.newTrack == "") {
      $scope.newTrack = "track field can't be empty!";
    }
    else {

      $scope.album.tracks.push(newTrackObject);

      $scope.newTrack = "";

      if ($scope.singleTrack == "true") {
        $scope.single = true;
      }
    }
  };

  $scope.deleteTrack = function ($track) {
    var index = $scope.album.tracks.indexOf($track);

    if (index > -1) {
      $scope.album.tracks.splice(index, 1);
    }

    if ($scope.singleTrack == "true") {
      $scope.single = false;
    }
  };

});
