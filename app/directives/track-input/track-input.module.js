/**
 * Created by aleksander on 04.03.17.
 */
'use strict';
angular.module('directives.trackInput', [])
  .directive('trackInput', function () {
    return {
      replace: true,
      restrict: 'E',
      templateUrl: 'directives/track-input/track-input.template.html',
      controller: 'trackInputController',

      scope: {
        album: '=ngModel',
        singleTrack: '@single'
      }

    };
  });
