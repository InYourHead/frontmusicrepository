/**
 * Created by aleksander on 05.03.17.
 */
describe('track-input directive test', function () {

  var $scope, $controller, $compile, ctrl, compiledEl, el;

  beforeEach(module('directives.trackInput'));
  beforeEach(module('templates'));


  beforeEach(inject(function (_$rootScope_, _$controller_, _$compile_) {

    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $compile = _$compile_;

    ctrl = _$controller_('trackInputController', {
      $scope: $scope
    });

    $scope.album = {albumTitle: 'Metallica', tracks: []};
    $scope.singleTrack = "true";

    el = angular.element('<track-input></track-input>');

    compiledEl = $compile(el)($scope);

    $scope.$digest();

  }));

  it('fails when element is not compiled', function () {

    expect($scope.album).toBeDefined();

    expect(compiledEl).toBeDefined();

    expect(compiledEl.html()).toContain('input');

  });

  it('fails when adding new track not work properly', function () {

    $scope.newTrack = "NewTrack";

    $scope.addTrack();

    expect($scope.album).toEqual({albumTitle: 'Metallica', tracks: [{trackTitle: 'NewTrack'}]});

  });

  it('fails when delete track not working properly', function () {

    var track = {trackTitle: 'NewTrack'};

    $scope.deleteTrack(track);

    expect($scope.album).toEqual({albumTitle: 'Metallica', tracks: []});

  });

  it('fails when input element not hiding after adding track (single attribute is true)', function () {

    $scope.newTrack = "NewTrack";

    $scope.addTrack();

    expect($scope.singleTrack).toEqual("true");

    expect($scope.single).toEqual(true);
  });

  it('fails when input element hiding after adding track (single attribute is false)', function () {

    $scope.singleTrack = "false";

    var el = angular.element('<track-input></track-input>');

    $compile(el)($scope);

    $scope.$digest();

    $scope.newTrack = "NewTrack";

    $scope.addTrack();

    expect($scope.singleTrack).toEqual("false");

  });

  it('fails when input not show up after delete track (single attribute is true)', function () {
    var track = {trackTitle: 'NewTrack'};

    $scope.deleteTrack(track);

    expect($scope.single).toEqual(false);
  });

});
