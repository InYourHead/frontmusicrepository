/**
 * Created by aleksander on 25.02.17.
 */
'use strict';
describe('searchBar', function () {

  var $rootScope, compile, $httpBackend;
  var compiledElem, element;

  beforeEach(module('directives.searchBar'));

  beforeEach(module('templates'));


  beforeEach(inject(function ($compile, _$rootScope_, $templateCache, _$httpBackend_) {

    compile = $compile;
    $rootScope=_$rootScope_.$new();

    $httpBackend = _$httpBackend_;

    $rootScope.artists = null;

    $rootScope.typedQuery = "Hello";

    $rootScope.searchQuery = null;

    //is called
    $httpBackend.expectGET('http://localhost:8080/artists').respond([{artistName: 'Metallica', albums: []}]);
    //is created
    element = angular.element('<search-bar  artists="artists" ng-model="typedQuery"></search-bar>');
    compiledElem = $compile(element)($rootScope);

    $rootScope.$digest();

    //var temp=compiledElem;

  }));


  it('pass when compiled directive is not equal to ""', function () {

    /*
    element = angular.element('<search-bar  artists="artists" ng-model="typedQuery"></search-bar>');
    compiledElem = compile(element)($rootScope);

    $rootScope.$digest();
     */  //but have no values, only object
    expect(compiledElem).not.toEqual('');
    expect(compiledElem.find('input')).toBeDefined();
  });


});
