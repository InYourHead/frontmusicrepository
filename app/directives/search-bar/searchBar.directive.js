/**
 * Created by aleksander on 22.02.17.
 */
'use strict';
angular.module('directives.searchBar')
    .directive('searchBar', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'directives/search-bar/search-bar.template.html',
            controller: 'searchBarController',
            controllerAs: 'ctrl',
            //bindujemy do kontrolera zmienne, dzięki czemu będziemy mogli dostać się do nich w kontrolerze za pomocą 'this'
            scope: {
                'artists': '=',
                'typedQuery':"=ngModel"

            }
        };
    });
