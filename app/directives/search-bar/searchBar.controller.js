/**
 * Created by aleksander on 23.02.17.
 */
angular.module('directives.searchBar')
  .controller('searchBarController', ['ArtistsService', '$scope', function (ArtistsService, $scope) {

    $scope.typedQuery = "";
    $scope.searchQuery = "";

    $scope.artists = ArtistsService.query();
    $scope.typedQuery = this.searchQuery;
    }]);
