/**
 * Created by aleksander on 23.02.17.
 */
'use strict';
angular.module('directives',[
  'directives.searchBar',
  'directives.trackInput',
  'directives.albumInput',
  'directives.artistInput',
  'directives.statusField'
]);
