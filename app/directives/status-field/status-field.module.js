/**
 * Created by aleksander on 09.03.17.
 */
'use strict';
angular.module('directives.statusField', [])
  .directive('statusField', function () {

    return {

      replace: true,
      restrict: 'E',
      templateUrl: 'directives/status-field/status-field.template.html',
      controller: 'statusFieldController',
      scope: {
          message: '=',
          messageType: '=',
          alertClass: '='
      }
    }


  });
