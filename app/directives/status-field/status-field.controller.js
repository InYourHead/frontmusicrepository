/**
 * Created by aleksander on 09.03.17.
 */
'use strict';
angular.module('directives.statusField')
  .controller('statusFieldController', ['$scope', function ($scope) {



  $scope.$watch('message', function (value) {
    //$scope.isVisible = value != "";
      $scope.isVisible = !($scope.message == undefined || $scope.message == "");
  });

      $scope.hide= function () {

        $scope.message="";
        $scope.isVisible=false;

      }

  }]);
