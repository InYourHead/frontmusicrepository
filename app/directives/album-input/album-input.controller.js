/**
 * Created by aleksander on 05.03.17.
 */
'use strict';
angular.module('directives.albumInput')
  .controller('albumInputController', ['$scope', function ($scope) {

    $scope.newAlbum = "";

    $scope.addAlbum = function () {

      var newAlbumObject = {albumTitle: $scope.newAlbum, tracks: []};

      if ($scope.newAlbum == "") {
        $scope.newAlbum = "album field can't be empty!";
      }
      else {
        $scope.artist.albums.push(newAlbumObject);
        $scope.newAlbum = "";

        if ($scope.singleAlbum == "true") {
          $scope.single = true;
        }
      }
    };

    $scope.deleteAlbum = function ($album) {

      var index = $scope.artist.albums.indexOf($album);


      if (index > -1) {
        $scope.artist.albums.splice(index, 1);
      }

      if ($scope.singleAlbum == "true") {
        $scope.single = false;
      }
    };

  }]);
