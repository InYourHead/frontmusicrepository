/**
 * Created by aleksander on 05.03.17.
 */
'use strict';
angular.module('directives.albumInput', ['directives.trackInput'])
  .directive('albumInput', function () {

    return {

      restrict: 'E',
      replace: true,
      controller: 'albumInputController',
      templateUrl: 'directives/album-input/album-input.template.html',
      scope: {
        artist: '=ngModel',
        singleAlbum: '@single'
      }
    }

  });
