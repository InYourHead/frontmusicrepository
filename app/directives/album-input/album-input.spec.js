/**
 * Created by aleksander on 05.03.17.
 */
describe('album-input directive test', function () {

  var $scope, $controller, $compile, ctrl, compiledEl, el;
  beforeEach(module('directives.albumInput'));
  beforeEach(module('templates'));


  beforeEach(inject(function (_$rootScope_, _$controller_, _$compile_) {

    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $compile = _$compile_;

    ctrl = _$controller_('albumInputController', {
      $scope: $scope
    });

    $scope.artist = {artistName: 'Metallica', albums: []};
    $scope.singleAlbum = "true";

    el = angular.element('<album-input></album-input>');

    compiledEl = $compile(el)($scope);

    $scope.$digest();

  }));

  it('fails when element is not compiled', function () {

    expect($scope.artist).toBeDefined();

    expect(compiledEl).toBeDefined();

    expect(compiledEl.html()).toContain('input');

  });

  it('fails when adding new album not work properly', function () {

    $scope.newAlbum = "NewAlbum";

    $scope.addAlbum();

    expect($scope.artist).toEqual({artistName: 'Metallica', albums: [{albumTitle: 'NewAlbum', tracks: []}]});

  });

  it('fails when delete album not working properly', function () {

    var album = {albumTitle: 'NewAlbum', tracks: []};

    $scope.deleteAlbum(album);

    expect($scope.artist).toEqual({artistName: 'Metallica', albums: []});

  });

  it('fails when input element not hiding after adding album (single attribute is true)', function () {

    $scope.newAlbum = "NewAlbum";

    $scope.addAlbum();

    expect($scope.singleAlbum).toEqual("true");

    expect($scope.single).toEqual(true);
  });

  it('fails when input element hiding after adding album (single attribute is false)', function () {

    $scope.singleAlbum = "false";

    var el = angular.element('<album-input></album-input>');

    $compile(el)($scope);

    $scope.$digest();

    $scope.newAlbum = "NewAlbum";

    $scope.addAlbum();

    expect($scope.singleAlbum).toEqual("false");

  });

  it('fails when input not show up after delete album (single attribute is true)', function () {
    var album = {albumTitle: 'NewAlbum', tracks: []};

    $scope.deleteAlbum(album);

    expect($scope.single).toEqual(false);
  });

});
