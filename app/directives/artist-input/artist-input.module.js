/**
 * Created by aleksander on 05.03.17.
 */
'use strict';
angular.module('directives.artistInput', ['directives.albumInput'])
  .directive('artistInput', function () {

    return {

      restrict: 'E',
      replace: true,
      controller: 'artistInputController',
      templateUrl: 'directives/artist-input/artist-input.template.html',
      scope: {
        artists: '=ngModel',
        singleArtist: '@single'
      }
    }

  });
