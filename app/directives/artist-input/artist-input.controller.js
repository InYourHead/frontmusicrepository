/**
 * Created by aleksander on 05.03.17.
 */
'use strict';
angular.module('directives.artistInput')
  .controller('artistInputController', ['$scope', function ($scope) {

    $scope.newArtist = "";

    $scope.addArtist = function () {

      var newArtistObject = {artistName: $scope.newArtist, albums: []};

      if ($scope.newArtist == "") {
        $scope.newArtist = "artist field can't be empty!";
      }
      else {
        $scope.artists.push(newArtistObject);
        $scope.newArtist = "";

        if ($scope.singleArtist == "true") {
          $scope.single = true;

        }
      }
    };

    $scope.deleteArtist = function ($artist) {

      var index = $scope.artists.indexOf($artist);


      if (index > -1) {
        $scope.artists.splice(index, 1);
      }
      else {
        $scope.artists = [];
      }

      if ($scope.singleArtist == "true") {
        $scope.single = false;
      }

    };

  }]);
