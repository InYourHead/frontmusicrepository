/**
 * Created by aleksander on 06.03.17.
 */
describe('artist-input directive test', function () {

  var $scope, $controller, $compile, ctrl, compiledEl, el;

  beforeEach(module('directives.artistInput'));
  beforeEach(module('templates'));


  beforeEach(inject(function (_$rootScope_, _$controller_, _$compile_) {

    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $compile = _$compile_;

    ctrl = _$controller_('artistInputController', {
      $scope: $scope
    });

    $scope.artists = [];
    $scope.singleArtist = "true";

    el = angular.element('<artist-input></artist-input>');

    compiledEl = $compile(el)($scope);

    $scope.$digest();

  }));

  it('fails when element is not compiled', function () {

    expect($scope.artists).toBeDefined();

    expect(compiledEl).toBeDefined();

    expect(compiledEl.html()).toContain('input');

  });

  it('fails when adding new artist not work properly', function () {

    $scope.newArtist = "NewArtist";

    $scope.addArtist();

    expect($scope.artists).toEqual([{artistName: 'NewArtist', albums: []}]);

  });

  it('fails when delete artist not working properly', function () {

    $scope.artists = [{artistName: 'NewArtist', albums: []}];

    var artist = {artistName: 'NewArtist', albums: []};
    $scope.deleteArtist(artist);

    expect($scope.artists).toEqual([]);

  });

  it('fail when input element not hiding after adding artist (single attribute is true)', function () {

    $scope.newArtist = "NewArtist";

    $scope.addArtist();

    expect($scope.singleArtist).toEqual("true");

    expect($scope.single).toEqual(true);
  });

  it('fails when input element hiding after adding artist (single attribute is false)', function () {

    $scope.singleArtist = "false";

    var el = angular.element('<artist-input></artist-input>');

    $compile(el)($scope);

    $scope.$digest();

    $scope.newArtist = "NewArtist";

    $scope.addArtist();

    expect($scope.singleArtist).toEqual("false");

  });

  it('fails when input not show up after delete artist (single attribute is true)', function () {
    $scope.artists = [{artistName: 'NewArtist', albums: []}];

    var artist = {artistName: 'NewArtist', albums: []};
    $scope.deleteArtist(artist);

    expect($scope.single).toEqual(false);
  });

});
