/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 24.02.17.
 */
'use strict';
angular.module('albumView')
  .controller('albumController', ['$scope', '$routeParams', 'AlbumsService', '$location', 'BaseControllerFactory', function ($scope, $routeParams, AlbumsService, $location, BaseControllerFactory) {

      var baseController= BaseControllerFactory.create($scope);

    var artistName = $routeParams.artistName;

    var albumTitle = $routeParams.albumTitle;

    $scope.artistId=artistName;
    $scope.albumId=albumTitle;

    $scope.artistName = artistName;

    $scope.albumTitle = albumTitle;

    $scope.album = AlbumsService.get({artistName: artistName, albumTitle: albumTitle});

    $scope.editRequest = function () {

      if ($scope.album.albumTitle == "") {
          baseController.notify("Album title can't be empty!", "danger");

      }
      else {
        if ($scope.album.tracks) {
          for (var i = 0; i < $scope.album.tracks.length; i++) {
            if ($scope.album.tracks[i].trackTitle == "") {

                baseController.notify("Track title can't be empty!", "danger");
              break;
            }
          }
        }
      }

      if (!$scope.message) {
        AlbumsService.update({artistName: artistName, albumTitle: albumTitle}, $scope.album).$promise.then(baseController.defaultSaveSuccessHandler, {});
        $scope.artistId= $scope.artistName;
        $scope.albumId= $scope.albumtitle;
      }
    };

    $scope.deleteRequest = function () {
      $scope.id = {artistName: artistName, albumTitle: albumTitle};
      AlbumsService.delete($scope.id);
      $location.path('/search');
    }

  }]);
