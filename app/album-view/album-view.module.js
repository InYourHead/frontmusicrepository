/**
 * Created by aleksander on 01.03.17.
 */
'use strict';
angular.module('albumView', [
  'services.albumsService',
    'baseController',
  'ngRoute'
]);
