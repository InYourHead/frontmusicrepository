/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 25.02.17.
 */
describe('albumViewController', function () {

  beforeEach(module('albumView'));

  var albumController, scope, routeParams, $httpBackend, $controller, data, $compile;

  beforeEach(inject(function (_$compile_, _$controller_, _$rootScope_, _$routeParams_, AlbumsService, _$httpBackend_, $templateCache) {

    data = {albumTitle: 'Metallica', tracks: []};

    $compile = _$compile_;
    $httpBackend = _$httpBackend_;
    $controller = _$controller_;

    scope = _$rootScope_.$new();
    routeParams = {artistName: 'Metallica', albumTitle: 'Metallica'};

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica/albums/Metallica')
      .respond(data);


    albumController = $controller('albumController', {
      $scope: scope,
      $routeParams: routeParams,
      AlbumsService: AlbumsService
    });

  }));

  it('should check does albumController is definied', function () {

    expect(albumController).toBeTruthy();

  });

  it('should check does artistName is equal to $routeParams.artistName and albumTitle is equal to $routeParams.albumTitle', function () {
    jasmine.addCustomEqualityTester(angular.equals);

    $httpBackend.flush();

    expect(scope.artistName).toEqual('Metallica');

    expect(scope.albumTitle).toEqual('Metallica');

  });

  it('should check does TracksService is returning album object', function () {

    jasmine.addCustomEqualityTester(angular.equals);

    $httpBackend.flush();

    expect(scope.album).toEqual(data);

  });

  it('should check does delete button works', function () {

    $httpBackend.expectDELETE('http://localhost:8080/artists/Metallica/albums/Metallica')
      .respond(200);

    scope.deleteRequest();

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

  it('should check does update button works', function () {

    $httpBackend.expectPUT('http://localhost:8080/artists/Metallica/albums/Metallica')
      .respond(200);

    scope.editRequest();

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

});
