/**
 * Created by aleksander on 01.03.17.
 */
angular.module('albumView')
  .component('albumView', {

    templateUrl: 'album-view/album-view.template.html',
    controller: 'albumController'
  });
