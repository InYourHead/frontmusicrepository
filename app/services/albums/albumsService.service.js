/**
 * Created by aleksander on 01.03.17.
 */
angular.module('services.albumsService')
  .factory('AlbumsService', ['$resource',
    function ($resource) {

      return $resource('http://localhost:8080/artists/:artistName/albums/:albumTitle', {
        artistName: '@artistName',
        albumTitle: '@albumTitle'
      }, {
        query: {
          method: 'GET',
          isArray: true
        },
        update: {
          method: 'PUT'

        },
        delete: {
          method: 'DELETE'
        }
      });

    }]);
