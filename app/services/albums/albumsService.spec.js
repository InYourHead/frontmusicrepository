/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 25.02.17.
 */
describe('TracksService test', function () {

  beforeEach(module('services.albumsService'));

  var $httpBackend, AlbumsService;


  var albums = [
    {
      albumTitle: 'Metallica', tracks: [
      {trackTitle: 'The Unforgiven'}]
    }];

  beforeEach(inject(function (_$httpBackend_, _AlbumsService_) {

    AlbumsService = _AlbumsService_;
    $httpBackend = _$httpBackend_;

    /* Fucking piece of shit, Object({id: 1} is not equal to Resource({id: 1}) !!!!!!!!!!!!!
     $httpBackend.expectGET('http://localhost:8080/artists')
     .respond(artists);

     $httpBackend.expectGET('http://localhost:8080/artists/Metallica')
     .respond(artists[0]); */
  }));

  it('should get albums array', function () {

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica/albums')
      .respond(albums);

    var result = AlbumsService.query({artistName: 'Metallica'});

    $httpBackend.flush();

    expect(result[0].artistName).toEqual(albums[0].artistName);

  });

  it('should get single album', function () {

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica/albums/Metallica')
      .respond(albums[0]);

    var result = AlbumsService.get({artistName: 'Metallica', albumTitle: 'Metallica'});

    $httpBackend.flush();

    expect(result.artistName).toEqual(albums[0].artistName);

  });
  it('should update single album', function () {

    $httpBackend.expectPUT('http://localhost:8080/artists/Metallica/albums/Metallica')
      .respond(200);

    AlbumsService.update({artistName: 'Metallica', albumTitle: 'Metallica'}, {});

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

  it('should delete single album', function () {


    $httpBackend.expectDELETE('http://localhost:8080/artists/Metallica/albums/Metallica')
      .respond(200);

    AlbumsService.delete({artistName: 'Metallica', albumTitle: 'Metallica'});

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  })

});
