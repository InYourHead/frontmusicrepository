/**
 * Created by aleksander on 21.02.17.
 */
'use strict';
angular.module('services', [
    'services.artistsService',
  'services.albumsService',
  'services.tracksService',
    'ngRoute',
    'ngResource'
]);
