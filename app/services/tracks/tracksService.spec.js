/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 01.03.17.
 */
/**
 * Created by aleksander on 25.02.17.
 */
describe('TracksService test', function () {

  beforeEach(module('services.tracksService'));

  var $httpBackend, TracksService;


  var tracks = [

    {trackTitle: 'The Unforgiven'}, {trackTitle: 'Curse'}];

  beforeEach(inject(function (_$httpBackend_, _TracksService_) {

    TracksService = _TracksService_;
    $httpBackend = _$httpBackend_;

    /* Fucking piece of shit, Object({id: 1} is not equal to Resource({id: 1}) !!!!!!!!!!!!!
     $httpBackend.expectGET('http://localhost:8080/artists')
     .respond(artists);

     $httpBackend.expectGET('http://localhost:8080/artists/Metallica')
     .respond(artists[0]); */
  }));

  it('should get tracks array', function () {

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica/albums/Metallica/tracks')
      .respond(tracks);

    var result = TracksService.query({artistName: 'Metallica', albumTitle: 'Metallica'});

    $httpBackend.flush();

    expect(result[0].trackTitle).toEqual(tracks[0].trackTitle);

  });

  it('should get single track', function () {

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica/albums/Metallica/tracks/Curse')
      .respond(tracks[1]);

    var result = TracksService.get({artistName: 'Metallica', albumTitle: 'Metallica', trackTitle: 'Curse'});

    $httpBackend.flush();

    expect(result.trackTitle).toEqual(tracks[1].trackTitle);

  });
  it('should update single track', function () {

    $httpBackend.expectPUT('http://localhost:8080/artists/Metallica/albums/Metallica/tracks/Curse')
      .respond(200);

    TracksService.update({artistName: 'Metallica', albumTitle: 'Metallica', trackTitle: 'Curse'});

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

  it('should delete single track', function () {


    $httpBackend.expectDELETE('http://localhost:8080/artists/Metallica/albums/Metallica/tracks/Curse')
      .respond(200);

    TracksService.delete({artistName: 'Metallica', albumTitle: 'Metallica', trackTitle: 'Curse'});

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  })

});
