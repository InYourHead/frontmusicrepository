/**
 * Created by aleksander on 01.03.17.
 */
angular.module('services.tracksService')
  .factory('TracksService', ['$resource',
    function ($resource) {
      return $resource('http://localhost:8080/artists/:artistName/albums/:albumTitle/tracks/:trackTitle', {
        artistName: '@artistName',
        albumTitle: '@albumTitle',
        trackTitle: '@trackTitle'
      }, {
        query: {
          method: 'GET',
          isArray: true
        },
        update: {
          method: 'PUT'
        },
        delete: {
          method: 'DELETE'
        }
      })
    }]);
