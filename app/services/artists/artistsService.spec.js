/**
 * Created by aleksander on 25.02.17.
 */
describe('ArtistsService test', function () {

  beforeEach(module('services.artistsService'));

  var $httpBackend, ArtistsService;



  var artists=[
    {artistName:'Metallica',albums:[
      {albumTitle: 'Metallica', tracks: [
        {trackTitle: 'The Unforgiven'}
        ]
      }]
    }];

  beforeEach(inject(function (_$httpBackend_, _ArtistsService_) {

    ArtistsService=_ArtistsService_;
    $httpBackend=_$httpBackend_;

    /* Fucking piece of shit, Object({id: 1} is not equal to Resource({id: 1}) !!!!!!!!!!!!!
    $httpBackend.expectGET('http://localhost:8080/artists')
      .respond(artists);

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica')
      .respond(artists[0]); */
  }));

  it('should get artist array', function () {

    $httpBackend.expectGET('http://localhost:8080/artists')
      .respond(artists);

    var result=ArtistsService.query();

    $httpBackend.flush();

    expect(result[0].artistName).toEqual(artists[0].artistName);

  });

  it('should get single artist', function () {

    $httpBackend.expectGET('http://localhost:8080/artists/Metallica')
      .respond(artists[0]);

    var result=ArtistsService.get({artistName: 'Metallica'});

    $httpBackend.flush();

    expect(result.artistName).toEqual(artists[0].artistName);

  });
  it('should update single album', function () {

    $httpBackend.expectPUT('http://localhost:8080/artists/Metallica')
      .respond(200);

    ArtistsService.update({artistName: 'Metallica'});

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  });

  it('should delete single artist', function () {


    $httpBackend.expectDELETE('http://localhost:8080/artists/Metallica')
      .respond(200);

    ArtistsService.delete({artistName: 'Metallica'});

    $httpBackend.flush();

    $httpBackend.verifyNoOutstandingRequest();

  })

});
