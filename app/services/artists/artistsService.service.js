/**
 * Created by aleksander on 20.02.17.
 */
'use strict';
angular.module('services.artistsService')
  .factory('ArtistsService', ['$resource',
    function ($resource) {
      return $resource('http://localhost:8080/artists/:artistName', {artistName: '@artistName'}, {

              update: {
                method: 'PUT'
              },
              delete: {
                method: 'DELETE'
              }

            });

        }]);
