/**
 * Created by aleksander on 25.02.17.
 */
angular.module('authInterceptor').factory('AuthInterceptor', ['$injector', 'AuthManager', '$location','$q' , function ($injector, AuthManager, $location, $q) {
  return {
    request: function (config) {

      var $http = $injector.get('$http');
      $http.defaults.headers.common = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      };
      //$http.defaults.headers.common[]='application/json';
      if (AuthManager.getEncodeCredentials()) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + AuthManager.getEncodeCredentials();
      }
          return config;
    },

    responseError: function (rejection) {

      if (rejection.status == 401) {

        AuthManager.deleteCredentials();
        $location.path('/login/errorCred');
      }
      else if (rejection.status == 403) {
        $location.path('/login/unauthorized');

      }
      else if (rejection.status == 404) {
          $location.path('/404');
          //  $location.path('/404.html');
      }

      else  if(rejection.status != 409) {
        $location.path('/404');

      }

      return $q.reject(rejection);

    }

  }
}]);
