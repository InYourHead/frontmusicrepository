/**
 * Created by aleksander on 26.02.17.
 */
describe('authInterceptor', function () {


  var $httpBackend, $location, AuthManager, AuthInterceptor;

  var mockedCred, mockedAuthManager;
  //mock factory
  beforeEach(function () {

    mockedCred = undefined;

    mockedAuthManager = {
      getEncodeCredentials: function () {
        return mockedCred;
      },
      setCredentials: function ($l, $p) {
        mockedCred = btoa($l + ':' + $p);
      },
      deleteCredentials: function () {
        mockedCred = undefined;
      }
    };
    //potrzebujemy mockować authManager
    //angular.module('authManager');

    module('authInterceptor');
    module('services.artistsService');

    module(function ($provide) {

      $provide.factory('AuthManager', function () {
        return mockedAuthManager;
      });

      var _path = '';
      //mock location
      locationMock = {
        path: function (argument) {
          if (argument) {
            _path = argument;
          }
          return _path;
        }
      };

      module(function ($provide) {
        $provide.value('$location', locationMock);
      });
    });

  });

  beforeEach(inject(function (_AuthManager_, _AuthInterceptor_, _$location_, _$httpBackend_) {

    AuthManager = _AuthManager_;
    AuthInterceptor = _AuthInterceptor_;
    $location = _$location_;
    $httpBackend = _$httpBackend_;
  }));


  it('should AuthManager be defined', function () {

    expect(AuthManager).toBeDefined();
  });

  it('fails when authInterceptor is not defined', function () {
    expect(AuthInterceptor).toBeDefined();
  });

  it('fails when user is not redirected to login page (status 401)', function () {
    var response = {
      status: 401,
      config: {}
    };
    AuthInterceptor.responseError(response);

    var loc = $location.path();
    expect(loc).toBe('/login/errorCred');
  });

  it('fails when user is not redirected to login page (status 403)', function () {
    var response = {
      status: 403,
      config: {}
    };
    AuthInterceptor.responseError(response);

    var loc = $location.path();
    expect(loc).toBe('/login/unauthorized');

  });

  it('fails when Accept header is not set (GET)', function () {

    $httpBackend.when('GET', 'http://localhost:8080/artists', null, function (headers) {
      expect(headers.Accept).toContain('application/json');
    }).respond(200);

  });

  it('fails when Accept header is not set (DELETE)', function () {

    $httpBackend.when('DELETE', 'http://localhost:8080/artists', null, function (headers) {
      expect(headers.Accept).toContain('application/json');
    }).respond(200);

  });

  it('fails when Accept and Content-Type headers are not set (POST)', function () {
    $httpBackend.when('POST', 'http://localhost:8080/artists', null, function (headers) {
      expect(headers.Accept).toContain('application/json');
      expect(headers.Content - Type).toContain('application/json');
    }).respond(200);
  });

  it('fails when Accept and Content-Type headers are not set (PUT)', function () {
    $httpBackend.when('PUT', 'http://localhost:8080/artists', null, function (headers) {
      expect(headers.Accept).toContain('application/json');
      expect(headers.Content - Type).toContain('application/json');
    }).respond(200);
  });

  it('fails when Authorization header is not set after user login', function () {

    //user is logged in
    AuthManager.setCredentials("login", "password");

    $httpBackend.when('GET', 'http://localhost:8080/artists', null, function (headers) {
      expect(headers.Authorization).toBeTruthy();
    }).respond(200);

  });

  it('fails when Authorization header is present and user is not logged in', function () {
    $httpBackend.when('GET', 'http://localhost:8080/artists', null, function (headers) {
      expect(headers.Authorization).toBeUndefined();
    }).respond(200);
  });

});
