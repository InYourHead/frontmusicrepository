/**
 * Created by aleksander on 26.02.17.
 */
'use strict';
angular.module('logoutController', ['authManager']).controller('logoutController', ['$location', 'AuthManager', function ($location, AuthManager) {


  AuthManager.deleteCredentials();

  $location.path('/');

}]);
