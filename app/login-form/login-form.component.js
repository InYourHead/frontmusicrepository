/**
 * Created by aleksander on 24.02.17.
 */
'use strict';
angular.module('loginForm').component('loginForm', {

  templateUrl: 'login-form/login-form.template.html',
  controller: 'loginController'

});
