/**
 * Created by aleksander on 24.02.17.
 */
'use strict';
angular.module('loginForm').controller('loginController', ['$scope', '$location', 'AuthManager', '$routeParams', function ($scope, $location, AuthManager, $routeParams) {




  var emptyJson= {
    login: '',
    password: ''
  };

  var labelTextHidden="show password";
  var labelTextVisible="hide password";

  var inputTypeText="text";
  var inputTypePass="password";


  //init values
  $scope.user=emptyJson;
  $scope.labelText=labelTextHidden;
  $scope.inputType=inputTypePass;
  $scope.isPasswordVisible=false;

  //error message
  if ($routeParams.error) {
    if ($routeParams.error == 'errorCred') {
      $scope.errorMessage = "Incorrect credentials or you are not logged in!"
    }
    else if ($routeParams.error == 'unauthorized') {
      $scope.errorMessage = "You have no acess to edit data!"
    }
  }


  //show/hide password
  $scope.toggleCheckbox=function (isPasswordVisible) {
    if(isPasswordVisible==true){

      $scope.labelText=labelTextVisible;
      $scope.inputType=inputTypeText;

    }
    else{
      $scope.labelText=labelTextHidden;
      $scope.inputType=inputTypePass;

    }
  };

  //send credentials to server
  $scope.submit = function () {

    if (($scope.user.login == "") || ($scope.user.password == "")) {

      AuthManager.deleteCredentials();

    }
    else {

      AuthManager.setCredentials($scope.user.login, $scope.user.password);
      $scope.encrypVal = AuthManager.getEncodeCredentials();

      $location.path("/search");

    }

  };

}]);
