/**
 * Created by aleksander on 25.02.17.
 */
'use strict';
angular.module('authManager').factory('AuthManager', ['$cookies', function ($cookies) {

  return {
    getEncodeCredentials: function () {
      var cred = $cookies.get('frontAppCredCookie');

        return cred;

    },

    setCredentials: function ($login, $passwd) {

      var encCred;
      if ((!$login) || (!$passwd)) {
        $cookies.remove('frontAppCredCookie');
      }
      else {

        encCred = btoa($login + ':' + $passwd);
        $cookies.put('frontAppCredCookie', encCred);
      }
    },

    deleteCredentials: function () {

      $cookies.remove('frontAppCredCookie');

    }
  };

}]);
