/**
 * Created by aleksander on 27.02.17.
 */
describe('AuthManager test', function () {

  var $cookies, AuthManager;
  var $mockValue, $mockKey;

  //mock $cookies service
  var mockCookies = {

    get: function ($key) {
      if ($key == $mockKey) return $mockValue;
    },
    put: function ($key, $value) {
      $mockKey = $key;
      $mockValue = $value;
    },
    remove: function ($key) {
      if ($mockKey == $key) $mockValue = undefined;
    }
  };

  beforeEach(function () {

    module('authManager');

    module('ngCookies', function ($provide) {
      $provide.service('$cookies', function () {
        return mockCookies;
      });
    });

  });

  //inject AuthManager
  beforeEach(inject(function ($injector, _AuthManager_) {

    $mockValue = undefined;
    $mockKey = undefined;
    AuthManager = _AuthManager_;

  }));

  describe('when mocking $cookies', function () {


    it('should have AuthManager been defined', function () {
      expect(AuthManager).toBeTruthy();
    });

    it('should AuthManager return cookies value', function () {
      $mockValue = '1234';
      $mockKey = 'frontAppCredCookie';
      expect(AuthManager.getEncodeCredentials()).toEqual('1234');
    });

    it('should set $cookie value to btoa(user:password)', function () {

      AuthManager.setCredentials('user', 'password');

      expect(AuthManager.getEncodeCredentials()).toEqual('dXNlcjpwYXNzd29yZA==');

    });

    it('should delete $cookie', function () {
      $mockValue = '1234';
      $mockKey = 'frontAppCredCookie';

      AuthManager.deleteCredentials();

      expect(AuthManager.getEncodeCredentials()).toEqual(undefined);

    })
  });


});
