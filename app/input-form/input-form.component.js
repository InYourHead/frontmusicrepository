/**
 * Created by aleksander on 02.03.17.
 */
'use strict';
angular.module('inputForm')
  .component('inputForm', {
    templateUrl: 'input-form/input-form.template.html',
    controller: 'inputFormController'
  });
