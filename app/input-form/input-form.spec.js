/**
 * Created by aleksander on 03.03.17.
 */
describe('inputForm test', function () {

  beforeEach(module('services'));
  beforeEach(module('inputForm'));

  var expectedArtistObject = {artistName: 'NewArtist', albums: []};
  var expectedArtistObjectWithAlbum = {artistName: 'NewArtist', albums: [{albumTitle: 'NewAlbum', tracks: []}]};

  var expectedArtistObjectWithAlbumAndTrack = {
    artistName: 'NewArtist',
    albums: [{albumTitle: 'NewAlbum', tracks: [{trackTitle: 'NewTrack'}]}]
  };

  var inputFormController, $location, BaseControllerFactory, $scope, $controller, $httpBackend, ArtistsService, AlbumsService, TracksService;

  beforeEach(inject(function (_$rootScope_, _$location_, _BaseControllerFactory_, _$controller_, _$httpBackend_, _ArtistsService_, _AlbumsService_, _TracksService_) {

    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $location = _$location_;
    BaseControllerFactory = _BaseControllerFactory_;
    $httpBackend = _$httpBackend_;
    ArtistsService=_ArtistsService_;
    AlbumsService=_AlbumsService_;
    TracksService=_TracksService_;

  }));


  describe('create Artist (/add)', function () {


    beforeEach(function () {


      inputFormController = $controller('inputFormController', {
        $scope: $scope,
        $location: $location,
        $routeParams: {},
          ArtistsService: ArtistsService,
          AlbumsService: AlbumsService,
          TracksService: TracksService,
        BaseControllerFactory: BaseControllerFactory
      });
    });

    it('inputFormController should be defined', function () {
      expect(inputFormController).toBeDefined();
    });

    it('should check does variables are initialized correctly', function () {

      expect($scope.artistName).toBeUndefined();

      expect($scope.albumTitle).toBeUndefined();

      expect(inputFormController.inputFormType).toEqual(null);

    });


    it('shouldCheck does submit() work', function () {

      $httpBackend.expectPOST('http://localhost:8080/artists').respond(200);

      $scope.artist = expectedArtistObjectWithAlbumAndTrack;

      $scope.submit();

      $httpBackend.flush();

      $httpBackend.verifyNoOutstandingRequest();

        expect($scope.childScope.message).toEqual("Save completed");


    });

  });

  describe('create album (/add/NewArtist)', function () {

    beforeEach(function () {


      inputFormController = $controller('inputFormController', {
        $scope: $scope,
        $location: $location,
        $routeParams: {artistName: 'NewArtist'},
          ArtistsService: ArtistsService,
          AlbumsService: AlbumsService,
          TracksService: TracksService,
        BaseControllerFactory: BaseControllerFactory
      });

      $httpBackend.expectGET('http://localhost:8080/artists/NewArtist').respond(200);
    });

    it('should check does variables are initialized correctly', function () {

      expect($scope.artistName).toEqual('NewArtist');

      expect($scope.albumTitle).toBeUndefined();

      expect(inputFormController.inputFormType).toEqual(null);

    });

    it('shouldCheck does submit() work', function () {

      $httpBackend.expectPOST('http://localhost:8080/artists/NewArtist/albums').respond(200);

      $scope.artist = expectedArtistObjectWithAlbumAndTrack;

      $scope.submit();

      $httpBackend.flush();

      $httpBackend.verifyNoOutstandingRequest();

        expect($scope.childScope.message).toEqual("Save completed");

    });

  });

  describe('create track (/add/NewArtist/NewAlbum)', function () {


    beforeEach(function () {

      inputFormController = $controller('inputFormController', {
        $scope: $scope,
        $location: $location,
        $routeParams: {artistName: 'NewArtist', albumTitle: 'NewAlbum'},
          ArtistsService: ArtistsService,
          AlbumsService: AlbumsService,
          TracksService: TracksService,
        BaseControllerFactory: BaseControllerFactory
      });

      $httpBackend.expectGET('http://localhost:8080/artists/NewArtist/albums/NewAlbum').respond(200);
    });


    it('should check does variables are initialized correctly', function () {


      expect($scope.artistName).toEqual('NewArtist');

      expect($scope.albumTitle).toEqual('NewAlbum');

      expect(inputFormController.inputFormType).toEqual(null);



    });

    it('shouldCheck does submit() work', function () {

      $httpBackend.expectPOST('http://localhost:8080/artists/NewArtist/albums/NewAlbum/tracks').respond(200);

      $scope.album = expectedArtistObjectWithAlbumAndTrack.albums[0];

      $scope.submit();

      $httpBackend.flush();

      $httpBackend.verifyNoOutstandingRequest();

        expect($scope.childScope.message).toEqual("Save completed");


    });

  });


});
