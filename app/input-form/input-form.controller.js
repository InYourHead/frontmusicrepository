/**
 * Created by aleksander on 02.03.17.
 */
'use strict';
angular.module('inputForm').controller('inputFormController', ['$scope', '$location', '$routeParams','ArtistsService', 'AlbumsService', 'TracksService' ,'BaseControllerFactory',
  function ($scope, $location, $routeParams, ArtistsService, AlbumsService, TracksService, BaseControllerFactory) {

    $scope.childScope = $scope.$new();
    var BaseController = BaseControllerFactory.create($scope.childScope);
    // if equal to 0- add artists 1- add albums to existing artist 2- add track to existing artist's album
    var inputFormType = this.inputFormType = null;

    $scope.newArtist = null;
    $scope.newAlbum = null;
    $scope.newTrack = null;


    $scope.artistName = $routeParams.artistName;

    $scope.albumTitle = $routeParams.albumTitle;


    //if artist or album does not exist, return /404
    if ($scope.artistName) {


      if ($scope.albumTitle) {

          AlbumsService.get( {artistName: $scope.artistName, albumTitle: $scope.albumTitle}).$promise.catch(function (error) {

                  if (error.status == 404) {
                      $location.path('/404');
                  }

              });

        inputFormType = 2;
        $scope.tracks = [];
      }
      else {

       ArtistsService.get( {artistName: $scope.artistName}).$promise.catch(function (error) {

              if (error.status == 404) {
                  $location.path('/404');
              }

          });

        $scope.albums = [];
        inputFormType = 1;
      }

    }
    else if ($scope.albumTitle && !$scope.artistName) {
      $location.path('/404.html');
    }
    else {
      inputFormType = 0;
    }

//initialize variables
    if (inputFormType == 0) {

      $scope.artist = [];

    }
    if (inputFormType == 1) {
      $scope.artist = {
        artistName: $scope.artistName,
        albums: []

      }
    }

    if (inputFormType == 2) {
      $scope.album = {
        albumTitle: $scope.albumTitle,
        tracks: []
      }

    }

    $scope.submit = function () {

      $scope.error = $scope.$parent.message;

      if (inputFormType == 0) {

        ArtistsService.save({artistName: null}, $scope.artist[0]).$promise.then(BaseController.defaultSaveSuccessHandler,BaseController.saveArtistErrorHandler);

      }
      else if (inputFormType == 1) {
        AlbumsService.save({
          artistName: $scope.artist.artistName,
          albumTitle: null
        }, $scope.artist.albums[0]).$promise.then(BaseController.defaultSaveSuccessHandler,BaseController.saveAlbumErrorHandler);
      }
      else {

        TracksService.save({
          artistName: $scope.artistName,
          albumTitle: $scope.album.albumTitle,
          trackTitle: null
        }, $scope.album.tracks[0]).$promise.then(BaseController.defaultSaveSuccessHandler,BaseController.saveTrackErrorHandler);
      }


    };
  }
]);
