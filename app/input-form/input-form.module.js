/**
 * Created by aleksander on 02.03.17.
 */
'use strict';
angular.module('inputForm', [
  'ngRoute',
  'baseController',
    'services',
  'directives.trackInput',
  'directives.albumInput',
  'directives.artistInput',
  'directives.statusField'
]);
