'use strict';

describe('frontApp e2e test', function () {

    it('should redirect `index.html` to `#/search`', function () {
        browser.get('index.html');
        expect(browser.getLocationAbsUrl()).toBe('/search/');
    });
});